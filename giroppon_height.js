var elevator;
var map;
var chart;
// var infowindow = new google.maps.InfoWindow();
var polyline;

// The following path marks a general path from Mt.
// Whitney, the highest point in the continental United
// States to Badwater, Death Valley, the lowest point.
var hills = new google.maps.LatLng(35.660439, 139.729184);
var chiba = new google.maps.LatLng(35.647515, 139.730254);

// Load the Visualization API and the columnchart package.
google.load('visualization', '1', {packages: ['columnchart']});

function initialize() {
    var mapOptions = {
zoom: 14,
      center: hills,
      mapTypeId: 'roadmap'
    }
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    // Create an ElevationService.
    elevator = new google.maps.ElevationService();

    // Draw the path, using the Visualization API and the Elevation service.
    drawPath(chiba);
    
    // Add a listener for the click event and call getElevation on that location
    google.maps.event.addListener(map, 'click', getElevation);

}

function drawPath(from) {

    // Create a new chart in the elevation_chart DIV.
    chart = new google.visualization.ColumnChart(document.getElementById('elevation_chart'));

    var path = [ from, hills];

    // Create a PathElevationRequest object using this array.
    // Ask for 256 samples along that path.
    var pathRequest = {
        'path': path,
        'samples': 256
    }

    // Initiate the path request.
    elevator.getElevationAlongPath(pathRequest, plotElevation);
}

// Takes an array of ElevationResult objects, draws the path on the map
// and plots the elevation profile on a Visualization API ColumnChart.
function plotElevation(results, status) {
    if (status != google.maps.ElevationStatus.OK) {
        return;
    }
    var elevations = results;

    // Extract the elevation samples from the returned results
    // and store them in an array of LatLngs.
    var elevationPath = [];
    for (var i = 0; i < results.length; i++) {
        elevationPath.push(elevations[i].location);
    }

    // Display a polyline of the elevation path.
    var pathOptions = {
        path: elevationPath,
        strokeColor: '#0000CC',
        opacity: 0.4,
        map: map
    }
    polyline = new google.maps.Polyline(pathOptions);

    // Extract the data from which to populate the chart.
    // Because the samples are equidistant, the 'Sample'
    // column here does double duty as distance along the
    // X axis.
    var lastElev = 0;
    var nowElev = 0;
    var total = 0;
    var maxElev = elevations[0].elevation;
    var minElev = elevations[0].elevation;
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Sample');
    data.addColumn('number', 'Elevation');
    for (var i = 0; i < results.length; i++) {
        data.addRow(['', elevations[i].elevation]);
        
        // 総標高差
        nowElev = elevations[i].elevation;
        total += Math.abs(nowElev - lastElev);
        lastElev = nowElev;i

        // 最大・小標高
        if(nowElev > maxElev){
            maxElev = nowElev;
         }
        if(nowElev < minElev){
            minElev = nowElev;
         }
    }
    console.log(total);
    var tmp = Math.abs(maxElev - minElev);
    total = Math.round(total * 10) / 10;
    maxElev = Math.round(maxElev * 10) / 10;
    minElev = Math.round(minElev * 10) / 10;
    tmp = Math.round(tmp * 10) / 10;
    document.getElementById('data').innerHTML = '総標高差 : '+total+'(m), 最大標高 : '+maxElev+'(m), 最低標高 : '+minElev+'(m), 最大標高差 : '+tmp+'(m)';

    // Draw the chart using the data within its DIV.
    document.getElementById('elevation_chart').style.display = 'block';
    chart.draw(data, {
        height: 150,
        legend: 'none',
        titleY: 'Elevation (m)'
    });
}


function getElevation(event) {

    var locations = [];

    // Retrieve the clicked location and push it on the array
    var clickedLocation = event.latLng;
    locations.push(clickedLocation);

    // Create a LocationElevationRequest object using the array's one value
    var positionalRequest = {
        'locations': locations
    }

    // Initiate the location request
    elevator.getElevationForLocations(positionalRequest, function(results, status) {
        if (status == google.maps.ElevationStatus.OK) {

            // Retrieve the first result
            if (results[0]) {

                // Open an info window indicating the elevation at the clicked position
                /*infowindow.setContent('The elevation at this point <br>is ' + results[0].elevation + ' meters.');
                infowindow.setPosition(clickedLocation);
                infowindow.open(map);*/
                deleteLine();
                polyline = null;
                drawPath(clickedLocation);
            } else {
                alert('No results found');
            }
        } else {
            alert('Elevation service failed due to: ' + status);
        }
    });
}

// 地図からラインを削除 
function deleteLine() 
{ 
    polyline.setMap(null); 
}

google.maps.event.addDomListener(window, 'load', initialize);

